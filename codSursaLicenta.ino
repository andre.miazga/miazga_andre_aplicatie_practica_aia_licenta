int senzor_umiditate = A1;
int activ_releu_pompa = 5;

int procentaj_umiditate_minim = 40;

unsigned long anteriorMilli = 0;
unsigned long secundaMilli = 1000;

int valoare_optima = 5;
int minute = 0;
int secunde = 0;

const int trigger = 7;
const int echo = 6;
float durata_receptie;
float distanta;
int inaltime_rezervor = 16;

void setup() {
  Serial.begin(9600);
 
  pinMode(activ_releu_pompa, OUTPUT);
  digitalWrite(activ_releu_pompa, LOW);

  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
}

void loop() {

  unsigned long curentMilli = millis();

  int umiditate_brut = analogRead(senzor_umiditate);
  
  int procentaj_umed = map(umiditate_brut, 0, 1023, 100, 0);
  Serial.print("Umiditatea solului: ");
  Serial.print(procentaj_umed);
  Serial.println("%");

  digitalWrite(trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  durata_receptie = pulseIn(echo, HIGH);
  distanta = durata_receptie * 0.034 / 2;

  Serial.print("Nivel Apa: ");
  Serial.print(distanta);
  Serial.println(" cm");

  if(curentMilli - anteriorMilli >= secundaMilli){
    anteriorMilli = curentMilli;

    secunde++;
    if(secunde == 60){
      minute++;
      secunde = 0;
    }
    Serial.print("Timp:");Serial.print(minute);Serial.print(" minute si ");Serial.print(secunde);Serial.println(" secunde");
  }

  if(distanta < inaltime_rezervor)
  {
      if(procentaj_umed < procentaj_umiditate_minim && minute == valoare_optima){
        digitalWrite(activ_releu_pompa, HIGH);
        Serial.println("Pompa activata!");
      }
      else{
        digitalWrite(activ_releu_pompa, LOW);
        //Serial.println("Pompa Dezactivata!");
      }
      
  }
  else{
    digitalWrite(activ_releu_pompa, LOW);
    Serial.println("Lipsa apa!");
  }

  if(minute >= valoare_optima){
          minute = 0;
      }
}
