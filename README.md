
Adresa: https://gitlab.upt.ro/andre.miazga/miazga_andre_aplicatie_practica_aia_licenta



Paşi:

1. Instalarea mediului de dezvoltare Arduino IDE. Se poate găsi pe siteul oficial Arduino https://www.arduino.cc/en/software
2. In mediul de dezvoltare Arduino IDE, selectati File -> Open -> daţi click pe codul sursă
3. Daţi click pe Verify pentru a compila codul sursa
4. Conectaţi microcontroller-ul Arduino la dispozitivul pe care il operaţi
5. Daţi click pe Upload